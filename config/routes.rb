Rails.application.routes.draw do

  devise_for :users
  resources :eventos do
        resources :reviews, except: [:show, :index]
  end
  get 'pages/sobre'

  get 'pages/contato'

  root 'eventos#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
